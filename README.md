# Debugging Tutorial

This repository serves as a tutorial for debugging the data and control planes. 

## Introduction

Please have a look at HW0 before reading this tutorial.

In this repository, we will explore trying to send a message between two hosts. The topology for this tutorial is pictured below:

![Topology](configs/topology.png)

### Getting Started

To start, first clone this repository. 

Let us try to send a message from `h1` to `h2`. First, run: `make run`. Two `xterm` instances should start up, one for each switch. 

On the `h2` instance, listen for incoming messages by running:

`./tail_pcap.sh pcaps/h2_in.pcap`

On the `h1` instance, send the message to `h2` by running:

`./send_ethernet.sh 00:00:00:00:02:02`

We should see that nothing new appears in the `h2` xterm window after sending the message from `h1`. We need to debug our code and figure out what is going wrong. The place to look is the `logs` folder.

### The logs Folders

When you run `make run`, a logs folder is created which contains a log file for the control plane and for each switch. In this case, below are the files we will find in the `logs` folder. 

- `cp.log` - This is the log folder for the control plane. Every print statement from the control plane goes to this file. 
- `s1.log` - This is the log file for switch 1. Events such as table entry insertions, table lookups, etc go to this file. 
- `s2.log` - This is the log file for switch 2. Events such as table entry insertions, table lookups, etc go to this file. 

After attempting to send the message from `h1` to `h2`, let us inspect the logs. We will first look at `s1.log` as the message should be sent from `s1` to `s2`. 

#### s1.log

At the start of the log, we will see log data related to the adding of table entries to `s1` from the control plane. These entries will start with the following code: 

```
updates {
  type: INSERT
```

These entries will not be useful to us. Scrolling down, we will encounter a log entry that contains `Match key:`. This indicates that a key is being matched against a table, which in our case means a packet is being processed. Let us take a closer look at the log entry:

```
[18:59:13.350] [bmv2] [D] [thread 6973] [0.0] [cxt 0] Table 'cis553Ingress.tiForward': hit with handle 1
[18:59:13.350] [bmv2] [D] [thread 6973] [0.0] [cxt 0] Dumping entry 1
Match key:
* hdr.ethernet.dstAddr: EXACT     000000000202
Action entry: cis553Ingress.aiForward - 1,

[18:59:13.350] [bmv2] [D] [thread 6973] [0.0] [cxt 0] Action entry is cis553Ingress.aiForward - 1,
[18:59:13.350] [bmv2] [T] [thread 6973] [0.0] [cxt 0] Action cis553Ingress.aiForward
[18:59:13.351] [bmv2] [T] [thread 6973] [0.0] [cxt 0] data_plane.p4(50) Primitive standard_metadata.egress_spec = egress_port
[18:59:13.351] [bmv2] [D] [thread 6973] [0.0] [cxt 0] Pipeline 'ingress': end
[18:59:13.351] [bmv2] [D] [thread 6973] [0.0] [cxt 0] Egress port is 1
[18:59:13.351] [bmv2] [D] [thread 6975] [0.0] [cxt 0] Pipeline 'egress': start
[18:59:13.351] [bmv2] [D] [thread 6975] [0.0] [cxt 0] Pipeline 'egress': end
[18:59:13.351] [bmv2] [D] [thread 6975] [0.0] [cxt 0] Deparser 'deparser': start
[18:59:13.352] [bmv2] [D] [thread 6975] [0.0] [cxt 0] Deparsing header 'ethernet'
[18:59:13.352] [bmv2] [D] [thread 6975] [0.0] [cxt 0] Deparser 'deparser': end
[18:59:13.352] [bmv2] [D] [thread 6978] [0.0] [cxt 0] Transmitting packet of size 42 out of port 1
[18:59:14.350] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Processing packet received on port 1
[18:59:14.350] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Parser 'parser': start
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Parser 'parser' entering state 'start'
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Extracting header 'ethernet'
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Parser state 'start' has no switch, going to default next state
[18:59:14.351] [bmv2] [T] [thread 6973] [1.0] [cxt 0] Bytes parsed: 6
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Parser 'parser': end
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Pipeline 'ingress': start
[18:59:14.351] [bmv2] [T] [thread 6973] [1.0] [cxt 0] Applying table 'cis553Ingress.tiForward'
[18:59:14.351] [bmv2] [D] [thread 6973] [1.0] [cxt 0] Looking up key:
* hdr.ethernet.dstAddr: 000000000202
```

The key parts for our purposes is the following:

```
Match key:
* hdr.ethernet.dstAddr: EXACT     000000000202
Action entry: cis553Ingress.aiForward - 1,
```

We are told the value of the key (in this case, the key to the tiForward table in the data plane is `hdr.ethernet.dstAddr`): 000000000202. Additionally, we are told what action was taken and with what parameter: `cis553Ingress.aiForward - 1`. In this case, we are doing `aiForward` with a parameter of `1`. The rest of the output for the log entry has more P4 details which are not needed now but are still useful. 

The result is interesting. It is saying that when the destination address for the ethernet packet is 000000000202 (host 2), we do `aiForward` with a parameter of 1, meaning the packet gets forwarded out of port 1. However, looking at our topology, we should be forwarding out of port 2 to reach `h2`, not 1. It appears that the table entry for host 2 is incorrect for switch 1 as for a given destination, we have the wrong action parameter.

To see why we are forwarding out of port 1, we will inspect the control plane, as that is responsible for adding table entries. Let us look at the log for the control plane:

#### cp.log

In the control plane, we defined a method, `printTableEntry`, that prints a given table entry. We call this each time we add a table entry. This method is purely for debugging purposes. Remember, all print statements in the control plane go to `cp.log`. Therefore, let us examine `cp.log`: 


```
Output for switch 1
---------------------------------
Adding to table id: 33582124
Matching Information: [field_id: 1
exact {
  value: "\000\000\000\000\001\001"
}
]
Action Information: action {
  action_id: 16816187
  params {
    param_id: 1
    value: "\000\001"
  }
}

---------------------------------
Adding to table id: 33582124
Matching Information: [field_id: 1
exact {
  value: "\000\000\000\000\002\002"
}
]
Action Information: action {
  action_id: 16816187
  params {
    param_id: 1
    value: "\000\001"
  }
}
```

This is the output from the `printTableEntry` method. In the control plane, we are adding two entries to switch 1: forwarding logic for `h1` and `h2`. In the first table entry, we see that for the key `\000\000\000\000\001\001` (`h1`) the value of `aiForward` is `\000\001`, which means we forward out of port 1. Moving to the second table entry, we see that for the key `\000\000\000\000\002\002` (`h2`) the value of `aiForward` is `\000\001`, which means we forward out of port 1. However, based on the topology image, we know this is incorrect as we should be forwarding out of port 2.  

Now that we idenfitied the problem, we can change the the control plane to fix our code. Namely we change:

```
table_entry = p4info_helper.buildTableEntry(
        table_name = "cis553Ingress.tiForward",
        match_fields = {"hdr.ethernet.dstAddr": "00:00:00:00:02:02"},
        action_name = "cis553Ingress.aiForward",
        action_params = {"egress_port": 1})
    sw.WriteTableEntry(table_entry)
```

to

```
table_entry = p4info_helper.buildTableEntry(
        table_name = "cis553Ingress.tiForward",
        match_fields = {"hdr.ethernet.dstAddr": "00:00:00:00:02:02"},
        action_name = "cis553Ingress.aiForward",
        action_params = {"egress_port": 2})
    sw.WriteTableEntry(table_entry)
```

### Verifying Our Fix

Once making the change, we type `exit` into mininet and run `make clean` to clear out all of the logs. We then run `make run` again to recompile our code. Next, we try the same steps above to send a message from `h1` and listen for messages on `h2`. 

On the `h2 xterm` we should see the following output now:

```
00:00:00:00:01:01 > 00:00:00:00:02:02, ethertype ARP (0x0806), length 42: Request who-has 0.0.0.0 tell 10.0.1.1, length 28
```

Indicating that h2 recieved the message! Finally, we can confirm our code works by once again looking at `s1.log`:

```
[19:32:00.188] [bmv2] [D] [thread 7343] [19.0] [cxt 0] Table 'cis553Ingress.tiForward': hit with handle 1
[19:32:00.188] [bmv2] [D] [thread 7343] [19.0] [cxt 0] Dumping entry 1
Match key:
* hdr.ethernet.dstAddr: EXACT     000000000202
Action entry: cis553Ingress.aiForward - 2,

[19:32:00.188] [bmv2] [D] [thread 7343] [19.0] [cxt 0] Action entry is cis553Ingress.aiForward - 2,
[19:32:00.188] [bmv2] [T] [thread 7343] [19.0] [cxt 0] Action cis553Ingress.aiForward
[19:32:00.188] [bmv2] [T] [thread 7343] [19.0] [cxt 0] data_plane.p4(50) Primitive standard_metadata.egress_spec = egress_port
[19:32:00.188] [bmv2] [D] [thread 7343] [19.0] [cxt 0] Pipeline 'ingress': end
[19:32:00.189] [bmv2] [D] [thread 7343] [19.0] [cxt 0] Egress port is 2
[19:32:00.189] [bmv2] [D] [thread 7346] [19.0] [cxt 0] Pipeline 'egress': start
[19:32:00.189] [bmv2] [D] [thread 7346] [19.0] [cxt 0] Pipeline 'egress': end
[19:32:00.189] [bmv2] [D] [thread 7346] [19.0] [cxt 0] Deparser 'deparser': start
[19:32:00.189] [bmv2] [D] [thread 7346] [19.0] [cxt 0] Deparsing header 'ethernet'
[19:32:00.189] [bmv2] [D] [thread 7346] [19.0] [cxt 0] Deparser 'deparser': end
[19:32:00.189] [bmv2] [D] [thread 7348] [19.0] [cxt 0] Transmitting packet of size 42 out of port 2
```

Looking at the key part:

```
* hdr.ethernet.dstAddr: EXACT     000000000202
Action entry: cis553Ingress.aiForward - 2
```

We see that when the destination address is host 2, we call `aiForward` with a parameter value of `2`, indicating that the message is forwarded out of port 2. 